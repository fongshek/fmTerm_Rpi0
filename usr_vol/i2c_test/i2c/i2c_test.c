#include "i2c_test.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


void gpioSetMode(unsigned gpio, unsigned mode);
int gpioRead(unsigned gpio);
void gpioWrite(unsigned gpio, unsigned level);


#define PI_INPUT  0
#define PI_OUTPUT 1
#define PI_ALT0   4
#define PI_ALT1   5
#define PI_ALT2   6
#define PI_ALT3   7
#define PI_ALT4   3
#define PI_ALT5   2




//#define I2C_START_DELAY()     sleep(0.001)
//#define I2C_STOP_DELAY()      sleep(0.001)

#define I2C_START_DELAY()     i2c_delay(200)
#define I2C_STOP_DELAY()      i2c_delay(200)

#define I2C_CLK_DELAY()       i2c_delay(10)

#define I2C_SCL_HI()          gpioWrite(GPIO_SCL,1)
#define I2C_SCL_LO()          gpioWrite(GPIO_SCL,0)

#define I2C_SDA_HI()          gpioWrite(GPIO_SDA,1)
#define I2C_SDA_LO()          gpioWrite(GPIO_SDA,0)

#define I2C_SDA_IN()          gpioSetMode(GPIO_SDA,PI_INPUT)     
#define I2C_SDA_OUT()         gpioSetMode(GPIO_SDA,PI_OUTPUT)     

#define I2C_SCL_IN()          gpioSetMode(GPIO_SCL,PI_INPUT)     
#define I2C_SCL_OUT()         gpioSetMode(GPIO_SCL,PI_OUTPUT)     


#define I2C_SDA()          gpioRead(GPIO_SDA)     
#define I2C_SCL()          gpioRead(GPIO_SCL)     



void i2c_delay(int loop)
{
      int i =0;
      while (i<loop)
      {
            i++;
      }

}

void i2c_clk(void)
{
#if(0)
      I2C_SCL_OUT();   
      I2C_CLK_DELAY();
      I2C_SCL_HI();
      I2C_CLK_DELAY();
      I2C_SCL_LO();
      I2C_CLK_DELAY();      
#endif

#if(1)
      I2C_SCL_IN();   
//      I2C_CLK_DELAY();
//      I2C_SCL_HI();
      I2C_CLK_DELAY();
      i2c_delay(1000);

      I2C_SCL_OUT();   
      I2C_SCL_LO();
      I2C_CLK_DELAY();   
      i2c_delay(1000);

#endif

}

int i2c_poll_scl(int timeout)
{
      int i = 0;

      while (i<timeout)
      {
            i ++;
            if (I2C_SCL() == 0)
            {
                  return(1);
            }

      }

      return (-1);
}

int i2c_poll_sda(int timeout)
{
      int i = 0;

      while (i<timeout)
      {
            i ++;
            if (I2C_SDA() == 0)
            {
                  return(1);
            }

      }

      return (-1);
}

int i2c_read_bit(void)
{
      int bit_read=0;
      I2C_SDA_IN();
      I2C_CLK_DELAY();

      i2c_delay(10);

      I2C_SCL_IN();
      I2C_CLK_DELAY();        

      i2c_delay(1000);


      I2C_SCL_OUT();   
      I2C_SCL_LO();

      bit_read = I2C_SDA();

      I2C_CLK_DELAY();        
      i2c_delay(1000);


      return (bit_read);

}

int i2c_write_ack(void)
{

      I2C_SDA_OUT();
      I2C_SDA_LO();
      i2c_clk();
      I2C_SDA_IN();
      return (0);

}

int i2c_read_ack(void)
{

      I2C_SDA_IN();
      i2c_clk();
//      I2C_SCL_IN();
//      i2c_delay(1000);
      return (0);

}

int i2c_read_byte(void)
{

      I2C_SDA_IN();
      int i;
      int byte_read = 0;
      int bit_read;
      for (i = 0; i<8; i++)
      {
            bit_read = i2c_read_bit();
            byte_read = byte_read << 1;
            if (bit_read)
                  byte_read |= 1;
      }

      i2c_write_ack();

      return (byte_read);
}

void i2c_write_bit(int data)
{

      I2C_SDA_OUT();
      if (data)
      {
            I2C_SDA_IN();
//            I2C_SDA_HI();
      }
      else
      {
            I2C_SDA_LO();
      }
      i2c_clk();
}

void i2c_write_byte(int data)
{
      I2C_SDA_OUT();
      int i;
      for (i = 0; i<8; i++)
      {
            i2c_write_bit(data & (1<<(7-i)));
      }

      i2c_read_ack();

}

void i2c_start(void)
{
      printf("i2c_start\n");
      I2C_SDA_OUT();
      i2c_delay(1000);
      I2C_SCL_OUT();
      i2c_delay(1000);
      I2C_SCL_HI();
      i2c_delay(1000);
      I2C_SDA_HI();
      I2C_START_DELAY();
      i2c_delay(1000);
      I2C_SDA_LO();
      I2C_START_DELAY();
      I2C_SCL_LO();
      i2c_delay(1000);

}

void i2c_stop(void)
{
     printf("i2c_stop\n");

      I2C_SDA_OUT();
      I2C_SCL_OUT();
      I2C_SCL_LO();
      I2C_SDA_LO();
      I2C_STOP_DELAY();
      I2C_SCL_HI();
      I2C_STOP_DELAY();
      I2C_SDA_HI();
      
}



// ---------------------------------------------------------------------------------------------------

#define I2C_CLK_DELAY_ 100

void i2c_clk_(int timeout)
{

      // Clock Stretching
      // SCL = LOW
      // SCL = Weak High
      // Wait for Slave to poll low SCL
      // timeout : 100 ~ 300kHz

      I2C_SCL_OUT();   
      I2C_SCL_LO();     // Strong Low
      i2c_delay(timeout);

      I2C_SCL_IN();     // Weak High
      i2c_delay(timeout);

      I2C_SCL_OUT();   
      I2C_SCL_LO();     // Strong Low


}

int i2c_write_ack_(int timeout)
{

      int ack;

      // Write ACK
      // SDA = weak high
      // SCL = strong low
      // delay
      // SDA = strong low
      // delay
      // SCL = weak high
      // delay
      // SCL = strong low
      // delay
      // SDA = weak high

      I2C_SDA_IN();           // SDA week high

      I2C_SCL_OUT();          // SCL strong low
      I2C_SCL_LO();
      i2c_delay(100);         // delay
      
      I2C_SDA_OUT();          // SDA strong low
      I2C_SDA_LO();
      i2c_delay(100);         // delay

      I2C_SCL_IN();           // SCL week high
      i2c_delay(100);         // delay


      I2C_SCL_OUT();          // SCL strong low
      I2C_SCL_LO();
      i2c_delay(100);         // delay
      I2C_SDA_IN();           // SDA week high

      return (0);

}

void i2c_read_bit_(int *data)
{
      // read data bit
      // SDA = weak high
      // SCL = strong low
      // Delay
      // SCL = weak high
      // Delay
      // Read SDA
      // SCL = strong low

      I2C_SDA_IN();           // SDA weak high


      I2C_SCL_OUT();
      I2C_SCL_LO();           // SCL strong low
      i2c_delay(100);

      I2C_SCL_IN();           // SCL weak high
      i2c_delay(100);
      *data = I2C_SDA();

      I2C_SCL_OUT();
      I2C_SCL_LO();           // SCL strong low
      i2c_delay(100);

}

int i2c_read_byte_(int *data)
{

      int ack;
      int i;
      *data = 0;
      int bit;

      for (i = 0; i<8; i++)
      {
            i2c_read_bit_(&bit);
            *data = (*data) << 1;
            if (bit)
                  *data += 1;
      }

      ack = i2c_write_ack_(100);           
      return (ack);


}


int i2c_read_ack_(int timeout)
{

      int ack;
      // Read ACK

      // SDA = weak high
      // SCL = strong low
      // delay
      // SCL = weak high
      // delay
      // wait for SDA to pull low by slave
      // timeout or SDA = low
      // SCL = strong low

      I2C_SDA_IN();           // SDA week high      

      I2C_SCL_OUT();          // SCL strong low
      I2C_SCL_LO();
      i2c_delay(100);     //

      I2C_SCL_IN();           // SCL week high
      i2c_delay(100);     //

      I2C_SCL_OUT();          // SCL strong low
      I2C_SCL_LO();
      i2c_delay(100);     //


#if(0)
      int loop = 0;

      do
      {

            ack = I2C_SDA(); // Read SDA ACK
            loop++;
            i2c_delay(10);

      } while ((loop < timeout) && (ack == 1));

      ack = I2C_SDA();        // Read SDA ACK

#endif


//      printf ("read NACK %02X %02X\n",loop,ack);

}


int i2c_read_ack__(int timeout)
{

      int ack;
      // Read ACK
      // SDA = weak high
      // SCL = strong low
      // delay
      // SCL = weak high
      // delay
      // wait for SDA to pull low by slave
      // timeout or SDA = low
      // SCL = strong low

      I2C_SDA_IN();           // SDA week high       
 //     ack = I2C_SDA();        // Read SDA ACK


//      i2c_delay (500);

//      I2C_SDA_IN();           // SDA week high       
      I2C_SCL_OUT();          // SCL strong low
      I2C_SCL_LO();
      i2c_delay(100);     //
      I2C_SCL_IN();           // SCL week high

      int loop=0;

      do{

            ack = I2C_SDA();        // Read SDA ACK
            loop ++;
            i2c_delay (10);

      }while ((loop < timeout) && (ack == 1));

      ack = I2C_SDA();        // Read SDA ACK


      I2C_SCL_OUT();
      I2C_SCL_LO();

//      printf ("read NACK %02X %02X\n",loop,ack);

}

void i2c_write_bit_(int data)
{
      // write data bit
      //
      // Bit 1 : SDA = Weak   high 
      // Bit 0 : SDA = Strong Low 
      //

      if (data)
      {
            I2C_SDA_IN();
      }
      else
      {
            I2C_SDA_OUT();
            I2C_SDA_LO();
      }
//      i2c_clk_(100);

      int timeout = 100;

      // Clock Stretching
      // SCL = LOW
      // SCL = Weak High
      // Wait for Slave to poll low SCL
      // timeout : 100 ~ 300kHz

      I2C_SCL_OUT();   
      I2C_SCL_LO();     // Strong Low
      i2c_delay(timeout);

      I2C_SCL_IN();     // Weak High
      i2c_delay(timeout);

      I2C_SCL_OUT();   
      I2C_SCL_LO();     // Strong Low


      i2c_delay(timeout);
//      I2C_SDA_IN();      

}


int i2c_write_byte_(int data)
{
      int ack;
      int i;
      for (i = 0; i<8; i++)
      {
            i2c_write_bit_(data & (1<<(7-i)));
      }

      ack = i2c_read_ack_(100);           
      return (ack);
}

void i2c_start_(int timeout)
{
//      printf("i2c_start\n");
      // I2C START condition
      // SDA weak high
      // SCL weak high
      // Delay
      // SDA strong low
      // Delay
      // SCL strong low
      // Delay


      I2C_SDA_IN();           // SDA weak high
      I2C_SCL_IN();           // SCL weak high
      i2c_delay(timeout);        // Delay

      I2C_SDA_OUT();
      I2C_SDA_LO();           // SDA strong low
      i2c_delay(timeout);

      I2C_SCL_OUT();
      I2C_SCL_LO();           // SCL strong low
      i2c_delay(timeout);

}

void i2c_stop_(int timeout)
{
      // I2C STOP condition
      // SCL strong low
      // Delay
      // SDA strong low
      // Delay
      // SCL weak high
      // Delay
      // SDA weak high
      // Delay


      I2C_SCL_OUT();
      I2C_SCL_LO();           // SCL strong low
      i2c_delay(timeout);
      i2c_delay(timeout);

      I2C_SDA_OUT();
      I2C_SDA_LO();           // SDA strong low
      i2c_delay(timeout);

 
      I2C_SCL_IN();           // SCL weak high
      i2c_delay(timeout);
      I2C_SDA_IN();           // SDA weak high

//      printf("i2c_stop\n");
      
}


int i2c_clk_stretching_poll(int timeout)
{
      // I2C Clock stretching polling
      // SCL = weak high
      // Poll SCL until Slave release the SCL line

      int loop = 0;
      int clk_in = 1;
      I2C_SCL_IN();           // SCL weak high
//      I2C_SDA_IN();           // SDA weak high
      i2c_delay(100);

      do{

            clk_in = I2C_SCL();     // Read I2C Clock
            loop++;
            i2c_delay(20);

      }while ((loop<timeout) && (clk_in ==0));

//      printf ("i2c clock stretching poll %02X %02x\n",clk_in,loop);

      return (loop);

}

int i2c_read_byte_cs(unsigned char *data, int ack_)
{
      *data = 0;
      int i;
      int bit;
      int timeout;

      // Poll for SCL release to weak high
      // Read 1st bit
      // generate 7 clocks to read 2-8 bits
      // write ack 

      timeout = i2c_clk_stretching_poll(10000);
      i2c_delay(400);      

      bit = I2C_SDA(); 

      *data = (*data) << 1;
      if (bit)
            *data += 1;            

      I2C_SCL_OUT();          
      I2C_SCL_LO();     //    SCL strong low
      i2c_delay(100);


      for (i=0; i<7; i++)
      {
            i2c_read_bit_(&bit);
            *data = (*data) << 1;
            if (bit)
                  *data += 1;            
      }

      int ack;
      if (ack_)
            ack = i2c_read_ack_(100);           
      else
            ack = i2c_write_ack_(100);           
      return (ack);

      
}


//#define I2C_ADDR 0x47
#define I2C_ADDR 0x40
#include <string.h>

int i2c_read_register(int slave_addr,int reg_addr, int len, unsigned char* data)
{

      int timeout;

      i2c_start_(100);                          // I2C start condition

      // I2C write register 

      i2c_write_byte_(slave_addr<<1);           // Slave Write
      i2c_delay(500);                           // Seperator for easy debugging
      i2c_write_byte_(reg_addr);                // Register value
      i2c_delay(500);                           // Seperator for easy debugging

      timeout = i2c_clk_stretching_poll(10000);

      i2c_delay(500);                           // Seperator for easy debugging
      i2c_start_(100);                          // I2C start condition

      // I2C read
      i2c_write_byte_((slave_addr<<1) | 1);       // Slave Read

      int i=0;
      int ack;
      for (i=0; i<len; i++)
      {
            ack = 0x00;
            if ((i + 1) == len)
                  ack = 0x01;
            i2c_read_byte_cs((data+i),ack);          // Slave Read with clock stretching     
      }


      I2C_SDA_OUT();
      I2C_SDA_LO();                             // SDA strong low

      timeout = i2c_clk_stretching_poll(10000);

      // STOP condition

      // Release SCL
      I2C_SCL_IN();                             // SDA weak high

      // Release SDA
      i2c_delay(500);
      I2C_SDA_IN();                             // SDA weak high

      return (0);
}


void i2c_test(void)
{

      int byte_read = 0x00;

      printf("i2c clock stretching Test %s %s \n",__DATE__,__TIME__);
      printf("i2c scl  0x%02x\n",GPIO_SCL);
      printf("i2c sda  0x%02x\n",GPIO_SDA);
      printf("i2c addr 0x%02x\n",I2C_ADDR);

      int i;
      unsigned char data[10];
      int ack;
      int timeout;

      memset (data,0xff,sizeof(data));

      i2c_read_register(I2C_ADDR,0x2F,3,&data[0]);

      printf("RX data : ");
      for (i=0 ; i<10; i++)
      {
            printf("%02X ",data[i]);
      }
      printf("\n");


#if(0)

      i2c_start_(100);                          // I2C start condition

      // I2C write register 

      i2c_write_byte_(I2C_ADDR<<1);             // Slave Write
      i2c_delay(500);                           // Seperator for easy debugging
      i2c_write_byte_(0x2F);                    // Register value
      i2c_delay(500);                           // Seperator for easy debugging


      timeout = i2c_clk_stretching_poll(10000);

      i2c_delay(500);                           // Seperator for easy debugging
      i2c_start_(100);                          // I2C start condition

      // I2C read
      

      i2c_write_byte_((I2C_ADDR<<1) | 1);       // Slave Read

      i2c_read_byte_cs(&data[0],0x00);          // Slave Read with clock stretching     
      i2c_read_byte_cs(&data[1],0x00);          // Slave Read with clock stretching     
      i2c_read_byte_cs(&data[2],0x01);          // Slave Read with clock stretching     

      I2C_SDA_OUT();
      I2C_SDA_LO();                             // SDA strong low

      timeout = i2c_clk_stretching_poll(10000);

      // STOP condition

      // Release SCL
      I2C_SCL_IN();                             // SDA weak high

      // Release SDA
      i2c_delay(500);
      I2C_SDA_IN();                             // SDA weak high


      printf("RX data : ");
      for (i=0 ; i<10; i++)
      {
            printf("%02X ",data[i]);
      }
      printf("\n");


#if(0)
      if (ack)
      {
            // For testing if clock stretch detected
            i2c_delay(100);
            I2C_SDA_OUT();
            I2C_SDA_LO();           // SDA strong low
            i2c_delay(1000);

            I2C_SDA_IN();           // SDA weak high
            i2c_delay(1000);

      }
#endif



#if(0)
      i2c_start_(100);

      i2c_write_byte_((I2C_ADDR<<1) | 1);
      i2c_read_byte_(&data[0]);
      i2c_delay(1000);

      i2c_read_byte_(&data[0]);
      i2c_delay(1000);

      i2c_read_byte_(&data[0]);
      i2c_delay(1000);
#endif

//      i2c_stop_(100);

#endif



#if(0)

      i2c_start();
      i2c_write_byte(I2C_ADDR<<1);
      i2c_write_byte(0x2F);

      gpioSetMode(GPIO_SDA,PI_INPUT);
      i2c_delay(5000);
      sleep(0.015);
//      I2C_SDA_HI();
      sleep(0.005);
//      i2c_start();

//    Start Condition
//      I2C_SDA_OUT();
//      I2C_SDA_HI();
      I2C_SCL_OUT();
      I2C_SCL_HI();
      sleep(0.005);
//      I2C_SDA_HI();
      I2C_START_DELAY();
      sleep(0.005);
      i2c_delay(1000);      
      I2C_SDA_OUT();
      sleep(0.005);
      i2c_delay(1000);      
      I2C_SDA_LO();
      sleep(0.005);
      i2c_delay(1000);      
      I2C_START_DELAY();
      sleep(0.005);
      i2c_delay(1000);      
      I2C_SCL_LO();
// ----------------

      sleep(0.005);
      i2c_delay(1000);      

      i2c_write_byte((I2C_ADDR<<1)+1);

      i2c_poll_sda(20000);
      i2c_delay(100);


      byte_read = i2c_read_byte();
//      printf("read byte :  %02X\n",byte_read);
      i2c_poll_sda(10000);
      i2c_delay(1000);


      byte_read = i2c_read_byte();
//      printf("read byte :  %02X\n",byte_read);
      i2c_poll_sda(10000);
      i2c_delay(1000);


      byte_read = i2c_read_byte();
//      printf("read byte :  %02X\n",byte_read);
      i2c_poll_sda(10000);
      i2c_delay(1000);


      i2c_stop();
#endif

}





