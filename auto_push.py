#!/usr/bin/python
from time import gmtime, strftime
import os, time,sys

# common utility


class cPRJ_BACKUP():

    def init(self):
        pass

    def run_shell(self,cmd):
        retStr = (os.popen(cmd).read()).replace("\n","")
        return (retStr)

    def insert2file(self,originalfile, string):
        with open(originalfile, 'r') as f:
            with open('newfile.txt', 'w') as f2:
                f2.write(string)
                f2.write(f.read())
        os.rename('newfile.txt', originalfile)

    def app2file(self,fname, content):
        with open(fname, "a") as myfile:
            myfile.write(content)

    def write2file(self,fname, content):
        with open(fname, "w") as myfile:
            myfile.write(content)

    def find_latest_file(self,path,extensions):
        print ("find_latest_file",path,extensions)
        mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
        sortedlist = list(sorted(os.listdir(path), key=mtime,reverse=True))
        file_names = [fn for fn in sortedlist if any([fn.endswith(extensions)])]

        if (0):
            for fn in sortedlist:
                if fn.endswith(".axf"):
                    print (fn)

        return (file_names[0])

    def find_map_summary(self,fname):
        print ("find_map_summary",fname)
        line_cap = ""
        with open(fname) as old_file:
            for line in old_file:
                if "Total RO  Size" in line:
                    line_cap += line
                if "Total RW  Size" in line:
                    line_cap += line
                if "Total ROM Size" in line:
                    line_cap += line

        return (line_cap)


    def get_map_summary(self):
        print ("get_map_summary")
        path = "keil\Listings"
        timestamp = (time.strftime("%y%m%d_%H%M%S"))
        fname = self.find_latest_file(path,"map")
        summary = self.find_map_summary(path+"\\"+fname)
        content = []
        content.append("#git autocommit {} {}".format(timestamp,fname.replace(".map","")))
        content.append(summary)
        content.append("# see change_notes.txt for change detail")

        fname = "..\commit_summary.txt"

        content_text =""
        for a in content:
            content_text += a + "\n"

        self.write2file(fname, content_text)

        fname = "..\commit.log"
        content_text = "{}\n".format(content[0])
        self.app2file(fname,content_text)
#        self.insert2file(fname,content_text)
        print (content_text)

        return(content)

    def git_autocommit(self):
        print ("git_autocommit")
        timestamp = (time.strftime("%y%m%d_%H%M%S"))

        ret = self.run_shell("git status")
        print (ret)

#        summary = self.get_map_summary()

#        fname = "commit_summary.txt"
        cmds = []
#        cmds.append("cd .. && git add -A")
#        cmds.append("cd .. && git commit -F {}".format(fname))
#        cmds.append("cd .. && git show -- summary")

        cmds.append("git add -A")
        cmds.append("git commit -m \"{}\"".format("see readme.txt"))
        cmds.append("git show -- summary")
        cmds.append("git push")

        if (1):
            for cmd in cmds:
                print (cmd)
                ret = self.run_shell(cmd)
                print (ret)


    def test(self):
        self.git_autocommit()
#        self.get_map_summary()
        if(0):
            print ("test")
            fname = find_latest_file("Listings","map")
            print (fname)
            pass



#test()
# git_autocommit()


def main():
    pass

    myDevice = cPRJ_BACKUP()

    myDevice.test()


    print ("Exit Main")

if __name__ == "__main__":
    main()