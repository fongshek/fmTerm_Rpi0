var http = require('http');

console.log ("Node Httpd:8080 HelloWorld");

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World!');
}).listen(8080);
