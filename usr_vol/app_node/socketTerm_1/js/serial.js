
exports.serial_write = function (data) {
    serial_write(data)
};

exports.callback = function (data) {
//    console.log(data)
    callback = data
};

exports.open = function (dev,baudrate) {

    console.log("open", dev, baudrate)
    open(dev,baudrate);
};


var callback

var isWin = process.platform === "win32"

const SerialPort = require('serialport');
if (isWin)
var serial_dev = "COM11";
else
var serial_dev = "/dev/ttyS11";

var baud = 115200;
var gSerial_RX = "";

//var serial_port = new SerialPort;
var serial_port;

//open (serial_dev,baud);


function open(serial_dev,baud)
{
    console.log ("connecting to %s:%d",serial_dev,baud);
    serial_port = new SerialPort(serial_dev, {
    baudRate: 115200
    });

    serial_port.on('open', () => console.log('Port open'));

    serial_port.on('data', function (data) {
    process_rx(data)
    });

}


// serial_port.write('ROBOT PLEASE RESPOND\n');

// Write to serial port
function serial_write(data)
{
//    console.log(data)
    serial_port.write(data)
}

// process RX data
function process_rx(data)
{
  rx_str = bin2string(data)
  gSerial_RX = gSerial_RX + rx_str 
  console.log('Data:', data,bin2string(data));
  if((gSerial_RX.endsWith("\r") == true) || (gSerial_RX.endsWith("\n") == true)) 
  {
    username = "~";

    var myBuffer=[];
    for(var i=0; i<gSerial_RX.length; i++) {
        myBuffer.push(gSerial_RX.charCodeAt(i))
    }
    broadcast_message(username,"binary data",myBuffer)

//    console.log('cmd:', gSerial_RX);
    broadcast_message(username,"new message",gSerial_RX)
    gSerial_RX = ""
  }
}

function bin2string(array){
	var result = "";
	for(var i = 0; i < array.length; ++i){
		result+= (String.fromCharCode(array[i]));
	}
	return result;
}

function broadcast_message(username , msg_type, message) {

//    console.log("broadcast",username,msg_type,message)
    callback(username,msg_type,message)
}
