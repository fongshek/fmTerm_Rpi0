#!/usr/bin/python

import sys
import time

print (sys.platform)

if sys.platform.startswith("win"):
    print ("User Win GPIO simulator")

    class GPIO:

        OUT     = 1
        IN      = 0
        HIGH    = 1
        LOW     = 0
        VERSION = "WIN GPIO simulator"
        DEBUG   = True

        def __init__(self):
            pass

        def setup(self, pin, state, initial=1):
            print("setup")
            if (self.DEBUG):
                print ("PIN_SETUP {}:{}:{}".format(pin,state,initial))

        def input(self, pin):
            if (self.DEBUG):
                print ("INPUT {}".format(pin))
            return (GPIO.HIGH)

        def output(self, pin, state):
            if (self.DEBUG):
                print ("OUTPUT {}:{}".format(pin,state))
            pass

        def test(self):
            print ("test")

else:
    print("Use RPi.GPIO")
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)


class cI2C_MASTER_CS:

    SYSTEM = sys.platform
    VERSION = "1.0"

    HIGH    = GPIO.HIGH
    LOW     = GPIO.LOW
    IN      = GPIO.IN
    OUT     = GPIO.OUT

    SDA     = 2
    SCL     = 3


    DELAY_START_CONDITION = 0.001
    DELAY_STOP_CONDITION = 0.001
    DELAY_CLK = 1

    def __init__(self, address, sda, scl, debug=False):
        print ("Init cI2C_MASTER_CS 0x{:02X} {} {} {}".format(address,sda,scl,debug))
#        GPIO.test()
        print ("GPIO_VERSION",GPIO.VERSION)
        self.I2C_ADDR = address
        self.DEBUG = debug
        self.SDA = sda
        self.SCL = scl
        self.init_bus()

# Utility function - Start

    def debug_print(self,log):
        if (self.DEBUG):
            print (log)

    def delay(self,val):
#        self.debug_print("delay {}".format(val))
        for i in range (0,val):
            pass
        if (False):        
            time.sleep(val)

# Utility function - end

    def init_bus(self):
        self.setup(self.SCL,self.IN)
        self.setup(self.SDA,self.IN)

# GPIO wrapper function - start

    def setup(self, pin, mode, initial = GPIO.HIGH):
        self.debug_print("SETUP {}:{}:{}".format(pin,mode,initial))
        GPIO.setup(pin,mode)

    def setup_output(self,pin,initial = GPIO.HIGH):
        GPIO.setup(pin,GPIO.OUT)
        GPIO.output(self.SDA, initial)         


    def input(self,pin):
        val = GPIO.input(pin)
        self.debug_print("INPUT {}:{}".format(pin,val))
        return (val)
    
    def output(self,pin,state):
        self.debug_print("INPUT {}:{}".format(pin,state))
        GPIO.output(pin,state)

# GPIO wrapper function - end


# I2C gpio wrapper - start

    def scl_input(self):
        self.setup(self.SCL,self.IN)

    def scl_output(self,initial):
#        self.setup(self.SCL,self.IN,initial)
#        self.scl_out(initial)
        self.setup_output(self.SCL,initial)

    def scl_in(self):
        val = self.input(self.SCL)
        self.debug_print("SCL_IN  : {}".format(val))
        return (val)

    def scl_out(self,val):
        self.debug_print("SCL_OUT : {}".format(val))
        if (val==self.LOW):
            self.output(self.SCL,GPIO.LOW)
        else:
            self.output(self.SCL,GPIO.HIGH)

    def sda_input(self):
        self.setup(self.SDA,self.IN)

    def sda_output(self,initial):
        self.setup_output(self.SDA,initial)        


    def sda_in(self):
        val = self.input(self.SDA)
        self.debug_print("SDA_IN  : {}".format(val))
        return (val)

    def sda_out(self,val):
        self.debug_print("SDA_OUT : {}".format(val))
        if (val==self.LOW):
            self.output(self.SDA,GPIO.LOW)
        else:
            self.output(self.SDA,GPIO.HIGH)

# I2C gpio wrapper - end

# I2C master clock stretching function - start

    def start_condition(self):
        timeout = 10
        self.scl_input()            # release SCL
        self.sda_input()            # release SDA
        self.delay(timeout)

        self.sda_output(self.LOW)   # SDA = LOW
        self.delay(timeout)         
        self.scl_output(self.LOW)   # SDA = LOW
        self.delay(timeout)


    def stop_condition(self):
#        self.debug_print("STOP_CONDITION")
        timeout = 10
        self.scl_input()            # release scl
        self.delay(timeout)
        self.sda_output(self.LOW)   # SDA = LOW
        self.delay(timeout) 
        self.sda_input()            # release SDA


    # -----------------------------------------------------
    # I2C write -------------------------------------------

    def write_byte_raw(self,val):
        for i in range(0,8):
            self.write_bit(val & (1<<(7-i)))
        ack = self.read_ack()     
        return(ack)  

    def write_bit(self,val):
        if (val == 0):
            self.sda_out(self.LOW)
        else:
            self.sda_input()
        
        timeout = 10
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 
        self.scl_input()                # SCL = weak high
        self.delay(timeout) 
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 

    def read_ack(self):
        self.sda_in()                   # SDA = weak high

        timeout = 10
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 
        self.scl_input()                # SCL = weak high
        self.delay(timeout) 
        val = self.sda_in()             # Read SDA
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 

        return(val)

    # I2C write -------------------------------------------
    # -----------------------------------------------------

    def clk_stretching_poll (self, timeout):
      # I2C Clock stretching polling
      # SCL = weak high
      # Poll SCL until Slave release the SCL line

        self.scl_input()                # SCL = weak high
        self.delay(10)

        loop = 0
        while True:
            clk_in = self.scl_in()      # read SCL
            loop = loop + 1
            self.delay(10)
            if ((loop < timeout) and (clk_in == self.LOW)):
                break

        return (loop)


    def i2c_read_byte_cs(self,  ack):
      # Poll for SCL release to weak high
      # Read 1st bit
      # generate 7 clocks to read 2-8 bits
      # write ack 

        data = 0
        timeout = self.clk_stretching_poll(10000)
        self.delay(100)

        bit = self.sda_in()

        data = data << 1
        if (bit != 0):
            data = data + 1 

        self.scl_output(self.LOW)           # SCL strong low
        self.delay(100)

        for i in range (0,7):
            bit = self.i2c_read_bit_()
            data = data << 1
            if (bit != 0):
                data = data + 1 

        if (ack == 0):
            self.write_ack(self.LOW)
        else:
            self.write_ack(self.HIGH)



    # -----------------------------------------------------
    # I2C read -------------------------------------------

    def read_byte_raw(self):
        byte_val = 0
        for i in range(0,8):
            byte_val = (byte_val << 1) 
            bit_val = self.read_bit()
            if (bit_val == GPIO.HIGH):
                byte_val = byte_val + 1
        ack = self.read_ack()                    
        return(byte_val)

    def read_bit(self):
        self.sda_input()                # SDA = Release

        timeout = 10
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 
        self.scl_input()                # SCL = weak high
        self.delay(timeout) 
        val = self.sda_in()             # Read SDA
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 

        return(val)

    def write_ack(self,ack):

        if (ack == 0):
            self.sda_output(self.LOW)
        else:
            self.sda_input()

        timeout = 10
        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 

        self.scl_input()                # SCL = weak high
        self.delay(timeout) 

        self.scl_output(self.LOW)       # SCL = LOW
        self.delay(timeout) 

        self.sda_input()                # release SDA


    # I2C read -------------------------------------------
    # -----------------------------------------------------




# -------------------------------------------------------------



    




# I2C master bitbang function - end(

# I2C master high levle function - 

    def write_i2c_block_data(self,addr,reg,data):
        self.start_condition()
        ack = self.write_byte_raw(addr)
        for i in range (0,len(data)):
            ack = self.write_byte_raw(addr)
        self.stop_condition()
        return (ack)


    def i2cScan(self,addr):
        detect = False
        reg = 0x00
        data = []        
        try:
            if self.SYSTEM.startswith("win"):
                detect = False
            else:
                detect = self.write_i2c_block_data(addr, reg, data)
        except:
            detect = False
        return(detect)   

    def i2cdetect(self):
        print ("I2C bitbang : {} : {} ".format(self.VERSION, self.SYSTEM) )
        print ("i2cdetect 0x{:02X} {} {}".format(self.I2C_ADDR,self.SDA,self.SCL))
        detect =[]
        for i in range (0x00,0x80):
            if (self.i2cScan(i)):
                detect.append(1)                
            else:
                detect.append(0)                

        print ("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f")
        for i in range (0,8):
            line =  "{:02X}:".format(i)
            for j in range (0,16):
                addr = (i * 16) + j
                if(detect[addr] == 0 ):
                    line = line + " --"                    
                else :
                    line = line + " {:02X}".format(addr)                    
            print (line)
        pass

    def test(self):
        print ("test")        
        if (0):
            GPIO.setup(2, GPIO.OUT) 
            GPIO.setup(3, GPIO.OUT) 
            for i in range(0,10):
                GPIO.output(2, GPIO.HIGH)
                GPIO.output(3, GPIO.HIGH)
                GPIO.output(2, GPIO.LOW)
                GPIO.output(3, GPIO.LOW)
        if (1):
            GPIO.setup(self.SCL,GPIO.IN)
            GPIO.setup(self.SDA,GPIO.IN)
            for i in range(0,10):
                GPIO.setup(self.SCL,GPIO.IN)
                GPIO.setup(self.SDA,GPIO.IN)
                
                if (0):
                    GPIO.setup(self.SCL,GPIO.OUT,GPIO.LOW)
                    GPIO.setup(self.SDA,GPIO.OUT,GPIO.LOW)

                if (1):
                    GPIO.setup(self.SCL,GPIO.OUT)
                    GPIO.setup(self.SDA,GPIO.OUT)

                    GPIO.output(self.SCL, GPIO.LOW)
                    GPIO.output(self.SDA, GPIO.LOW)
            
            GPIO.setup(self.SCL,GPIO.IN)
            GPIO.setup(self.SDA,GPIO.IN)
            self.delay(100)

        if (1):
            timeout = 10
            self.start_condition()
            self.stop_condition()

            if (0):
                self.scl_input()            # release SCL
                self.sda_input()            # release SDA
                self.delay(timeout)

    #            GPIO.setup(self.SDA,GPIO.OUT)
    #            GPIO.output(self.SDA, GPIO.LOW) 
                self.setup_output(self.SDA,GPIO.LOW)
    #            self.sda_output(self.LOW)   # SDA = LOW
                self.delay(timeout)         
    #            self.scl_output(self.LOW)   # SDA = LOW

                GPIO.setup(self.SCL,GPIO.OUT)
                GPIO.output(self.SCL, GPIO.LOW) 

                self.delay(timeout)


                timeout = 10
                self.scl_input()            # release scl
                self.delay(timeout)
                GPIO.setup(self.SDA,GPIO.OUT)
                GPIO.output(self.SDA, GPIO.LOW) 
                self.delay(timeout) 
                self.sda_input()            # release SDA




if __name__ == '__main__':
#  print ("self test")
  try:
    i2c = cI2C_MASTER_CS(address=0x48,sda=2,scl=3,debug=False)
#    i2c.i2cdetect()
    i2c.test()
  except:
    print "Error accessing default I2C bus"

