# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

---
## Prerequisite

This is a docker file for the development of the 

clock-stretching I2C 

```
All the lib. required to run the i2c driver (C and python)
has been installed in the docker image
The source is located at usr_vol which is outside the
docker container. and mounted as usr_vol inside the docker container

The source code of the I2C driver is located at

usr_vol/i2c_test


To run the docker container
user

docker-compose up



```

## [0.0.0] - 2018-08-23

* Add readme.md
*



