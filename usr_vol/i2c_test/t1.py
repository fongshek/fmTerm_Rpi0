import RPi.GPIO 
import time

myGPIO = GPIO()

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

for i in range(0,10):

	GPIO.output(2, GPIO.HIGH)
	GPIO.output(3, GPIO.HIGH)
#	time.sleep(0.01)
	GPIO.output(2, GPIO.LOW) 
	GPIO.output(3, GPIO.LOW)
