// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

var mysocket;

var exec = require('child_process').exec;

exec ("pwd",function(err,stdout,stderr){
    retStr = stdout.toString('utf8');
    console.log(retStr);
})

// wait ms milliseconds
function wait(ms) {
  return new Promise(r => setTimeout(r, ms));
}

async function hello() {
//  retStr = stdout.toString('utf8')
  retStr = ""
  console.log ("Hello ",retStr)
//  await exec_cmd("ls")
//  await wait(500);
//  retStr = stdout.toString('utf8')
  console.log ("World",retStr)
  return 'world';
}

hello()


async function sh(cmd) {
  return new Promise(function (resolve, reject) {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      } else {
        resolve({ stdout, stderr });
      }
    });
  });
}

async function main() {
  let { stdout } = await sh('ls');
  for (let line of stdout.split('\n')) {
    console.log(`ls: ${line}`);
  }
}

async function exec_sh(cmd) {
  username = "."
  let { stdout } = await sh(cmd);
  for (let line of stdout.split('\n')) {
//    console.log(`${cmd} : ${line}`);
    console.log(line);

    if (line != "")
    {
      mysocket.emit('new message', {
      username: username,
      message: line
      });

      mysocket.broadcast.emit('new message', {
        username: username,
        message: line
      });

    }    
  }
}



server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));

// Exec command
function exec_cmd(cmd)
{
    exec_sh (cmd);
};

// Chatroom

var numUsers = 0;

io.on('connection', (socket) => {
  var addedUser = false;

  mysocket = socket;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // we tell the client to execute 'new message'
  console.log(data);
  exec_cmd(data)
  socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });

  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username) => {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});
