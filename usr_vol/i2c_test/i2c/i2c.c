/*
   tiny_gpio.c
   2016-04-30
   Public Domain
*/

/*
   gcc -o tiny_gpio tiny_gpio.c
   ./tiny_gpio
*/

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#define GPSET0 7
#define GPSET1 8

#define GPCLR0 10
#define GPCLR1 11

#define GPLEV0 13
#define GPLEV1 14

#define GPPUD     37
#define GPPUDCLK0 38
#define GPPUDCLK1 39

unsigned piModel;
unsigned piRev;

static volatile uint32_t  *gpioReg = MAP_FAILED;

#define PI_BANK (gpio>>5)
#define PI_BIT  (1<<(gpio&0x1F))

/* gpio modes. */

#define PI_INPUT  0
#define PI_OUTPUT 1
#define PI_ALT0   4
#define PI_ALT1   5
#define PI_ALT2   6
#define PI_ALT3   7
#define PI_ALT4   3
#define PI_ALT5   2

void gpioSetMode(unsigned gpio, unsigned mode)
{
   int reg, shift;

   reg   =  gpio/10;
   shift = (gpio%10) * 3;

   gpioReg[reg] = (gpioReg[reg] & ~(7<<shift)) | (mode<<shift);
}

int gpioGetMode(unsigned gpio)
{
   int reg, shift;

   reg   =  gpio/10;
   shift = (gpio%10) * 3;

   return (*(gpioReg + reg) >> shift) & 7;
}

/* Values for pull-ups/downs off, pull-down and pull-up. */

#define PI_PUD_OFF  0
#define PI_PUD_DOWN 1
#define PI_PUD_UP   2

void gpioSetPullUpDown(unsigned gpio, unsigned pud)
{
   *(gpioReg + GPPUD) = pud;

   usleep(20);

   *(gpioReg + GPPUDCLK0 + PI_BANK) = PI_BIT;

   usleep(20);
  
   *(gpioReg + GPPUD) = 0;

   *(gpioReg + GPPUDCLK0 + PI_BANK) = 0;
}

int gpioRead(unsigned gpio)
{
   if ((*(gpioReg + GPLEV0 + PI_BANK) & PI_BIT) != 0) return 1;
   else                                         return 0;
}

void gpioWrite(unsigned gpio, unsigned level)
{
   if (level == 0) *(gpioReg + GPCLR0 + PI_BANK) = PI_BIT;
   else            *(gpioReg + GPSET0 + PI_BANK) = PI_BIT;
}

void gpioTrigger(unsigned gpio, unsigned pulseLen, unsigned level)
{
   if (level == 0) *(gpioReg + GPCLR0 + PI_BANK) = PI_BIT;
   else            *(gpioReg + GPSET0 + PI_BANK) = PI_BIT;

   usleep(pulseLen);

   if (level != 0) *(gpioReg + GPCLR0 + PI_BANK) = PI_BIT;
   else            *(gpioReg + GPSET0 + PI_BANK) = PI_BIT;
}

/* Bit (1<<x) will be set if gpio x is high. */

uint32_t gpioReadBank1(void) { return (*(gpioReg + GPLEV0)); }
uint32_t gpioReadBank2(void) { return (*(gpioReg + GPLEV1)); }

/* To clear gpio x bit or in (1<<x). */

void gpioClearBank1(uint32_t bits) { *(gpioReg + GPCLR0) = bits; }
void gpioClearBank2(uint32_t bits) { *(gpioReg + GPCLR1) = bits; }

/* To set gpio x bit or in (1<<x). */

void gpioSetBank1(uint32_t bits) { *(gpioReg + GPSET0) = bits; }
void gpioSetBank2(uint32_t bits) { *(gpioReg + GPSET1) = bits; }

unsigned gpioHardwareRevision(void)
{
   static unsigned rev = 0;

   FILE * filp;
   char buf[512];
   char term;
   int chars=4; /* number of chars in revision string */

   if (rev) return rev;

   piModel = 0;

   filp = fopen ("/proc/cpuinfo", "r");

   if (filp != NULL)
   {
      while (fgets(buf, sizeof(buf), filp) != NULL)
      {
         if (piModel == 0)
         {
            if (!strncasecmp("model name", buf, 10))
            {
               if (strstr (buf, "ARMv6") != NULL)
               {
                  piModel = 1;
                  chars = 4;
               }
               else if (strstr (buf, "ARMv7") != NULL)
               {
                  piModel = 2;
                  chars = 6;
               }
               else if (strstr (buf, "ARMv8") != NULL)
               {
                  piModel = 2;
                  chars = 6;
               }
            }
         }

         if (!strncasecmp("revision", buf, 8))
         {
            if (sscanf(buf+strlen(buf)-(chars+1),
               "%x%c", &rev, &term) == 2)
            {
               if (term != '\n') rev = 0;
            }
         }
      }

      fclose(filp);
   }
   return rev;
}

int gpioInitialise(void)
{
   int fd;

   piRev = gpioHardwareRevision(); /* sets piModel and piRev */

   fd = open("/dev/gpiomem", O_RDWR | O_SYNC) ;

   if (fd < 0)
   {
      fprintf(stderr, "failed to open /dev/gpiomem\n");
      return -1;
   }

   gpioReg = (uint32_t *)mmap(NULL, 0xB4, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

   close(fd);

   if (gpioReg == MAP_FAILED)
   {
      fprintf(stderr, "Bad, mmap failed\n");
      return -1;
   }
   return 0;
}

#define GPIO_SDA 2
#define GPIO_SCL 3
#define I2C_ADDR_KLEERENT 0x40

 #include <stdio.h>

void i2c_test(void);

#include "i2c_test.h"

int main(int argc, char *argv[])
{

      printf ("test %d\n",argc);

      printf("\nexe name=%s", argv[0]);

      int i;
      for (i=1; i< argc; i++) {
      printf("\narg%d=%s", i, argv[i]);
      }

      printf("\n");

   if (gpioInitialise() < 0) return 1;

   printf("Pi model = %d, Pi revision = %d\n", piModel, piRev);


      i2c_test();
      return (0);
}

#if(0)

#include<stdio.h>
#include<stdlib.h>

//#define I2C_START_DELAY()     sleep(0.001)
//#define I2C_STOP_DELAY()      sleep(0.001)

#define I2C_START_DELAY()     i2c_delay(500)
#define I2C_STOP_DELAY()      i2c_delay(500)

#define I2C_CLK_DELAY()       i2c_delay(500)

#define I2C_SCL_HI()          gpioWrite(GPIO_SCL,1)
#define I2C_SCL_LO()          gpioWrite(GPIO_SCL,0)

#define I2C_SDA_HI()          gpioWrite(GPIO_SDA,1)
#define I2C_SDA_LO()          gpioWrite(GPIO_SDA,0)

#define I2C_SDA_IN()          gpioSetMode(GPIO_SDA,PI_INPUT);     
#define I2C_SDA_OUT()         gpioSetMode(GPIO_SDA,PI_OUTPUT);     

#define I2C_SCL_IN()          gpioSetMode(GPIO_SCL,PI_INPUT);     
#define I2C_SCL_OUT()         gpioSetMode(GPIO_SCL,PI_OUTPUT);     


void i2c_delay(int loop)
{
      int i =0;
      while (i<loop)
      {
            i++;
      }

}

void i2c_clk(void)
{
      I2C_SCL_OUT();   
      I2C_CLK_DELAY();
      I2C_SCL_HI();
      I2C_CLK_DELAY();
      I2C_SCL_LO();

}

int i2c_read_bit(void)
{
      I2C_SDA_IN();
      i2c_clk();

}

int i2c_read_ack(void)
{

      I2C_SDA_IN();
      i2c_clk();
//      I2C_SCL_IN();
//      i2c_delay(1000);

}

int i2c_read_byte(void)
{

      I2C_SDA_IN();

      for (int i = 0; i<8; i++)
      {
            i2c_read_bit();
      }

      i2c_read_ack();

}

void i2c_write_bit(int data)
{

      I2C_SDA_OUT();
      if (data)
      {
            I2C_SDA_HI();
      }
      else
      {
            I2C_SDA_LO();
      }
      i2c_clk();
}

void i2c_write_byte(int data)
{
      I2C_SDA_OUT();

      for (int i = 0; i<8; i++)
      {
            i2c_write_bit(data & (1<<(7-i)));
      }

      i2c_read_ack();

}

void i2c_start(void)
{
      printf("i2c_start\n");
      I2C_SDA_OUT();
      I2C_SCL_OUT();
      I2C_SCL_HI();
      I2C_SDA_HI();
      I2C_START_DELAY();
      I2C_SDA_LO();
      I2C_START_DELAY();
      I2C_SCL_LO();

}

void i2c_stop(void)
{
     printf("i2c_stop\n");

      I2C_SDA_OUT();
      I2C_SCL_OUT();
      I2C_SCL_LO();
      I2C_SDA_LO();
      I2C_STOP_DELAY();
      I2C_SCL_HI();
      I2C_STOP_DELAY();
      I2C_SDA_HI();
      
}




void i2c_test(void)
{

      printf("i2c clock stretching Test \n");
      printf("i2c scl  0x%02x\n",GPIO_SCL);
      printf("i2c sda  0x%02x\n",GPIO_SDA);
      printf("i2c addr 0x%02x\n",I2C_ADDR_KLEERENT);

      i2c_start();
      i2c_write_byte(0x80);
      i2c_write_byte(0x2F);

      gpioSetMode(GPIO_SDA,PI_INPUT);
//      i2c_delay(5000);
      sleep(0.01);

      i2c_start();
      i2c_write_byte(0x81);
      sleep(0.02);
      i2c_read_byte();
      sleep(0.02);
      i2c_read_byte();
      sleep(0.02);
      i2c_read_byte();

      i2c_stop();

}

#endif






