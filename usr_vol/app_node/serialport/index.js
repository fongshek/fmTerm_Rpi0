/* eslint-disable node/no-missing-require */
'use strict';

// Use a Readline parser

const SerialPort = require('serialport');
const parsers = SerialPort.parsers;

// Use a `\r\n` as a line terminator
const parser = new parsers.Readline({
  delimiter: '\r'
});

var serial_dev = "/dev/ttyS11";
var baud = 115200;

console.log ("connecting to %s:%d",serial_dev,baud);
const port = new SerialPort(serial_dev, {
  baudRate: 115200
});

port.pipe(parser);

port.on('open', () => console.log('Port open'));

parser.on('data', console.log);

port.write('ROBOT PLEASE RESPOND\n');

// The parser will emit any string response
