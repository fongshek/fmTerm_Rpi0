#FROM resin/raspberry-pi-node:7-slim
FROM resin/raspberry-pi-python

RUN apt-get update

RUN apt-get install -yq dropbear build-essential \ 
    && apt-get install -y git libraspberrypi-bin python-smbus \
    && apt-get install -y i2c-tools \
    && apt-get install -y nano python curl wget net-tools socat


RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN echo 'root:toor' | chpasswd

# Defines our working directory in container
WORKDIR /app

# Copies the package.json first for better cache on later pushes
#COPY package.json package.json

# This install npm dependencies on the resin.io build server,
# making sure to clean up the artifacts it creates in order to reduce the image size.
#RUN JOBS=MAX npm install --production
# This will copy all files in our root to the working  directory in the container
#COPY . ./



# Create the default data directory
#RUN ln -s 

# server.js will run when container starts up on the device
#CMD ["npm", "start"]
